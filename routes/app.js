var express = require('express');
var router = express.Router();
var User = require('../models/user');

router.get('/', function (req, res, next) {
    User.findOne({}, function (err, doc) { //{} means first it will find
        if (err){
            return res.send('Error')
        }
        res.render('node', {email: doc.email});
    });
});

router.post('/', function (req, res, next) {
    var email = req.body.email;
    var user = new User({
        firstName: 'Igor',
        lastName: 'Boriskin',
        password: 'pass123',
        email: email
    });
    user.save();
    res.redirect('/');
});

module.exports = router;
